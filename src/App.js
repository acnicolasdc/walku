import React from 'react';
import NavigationOptions from './shareds/navigation';
import { createAppContainer} from "react-navigation";
class App extends React.Component {

  render() {
    const RootStack = createAppContainer(NavigationOptions('Splash'));
    return (
     
        <RootStack/>
       
    );
  }
}

export default (App);
