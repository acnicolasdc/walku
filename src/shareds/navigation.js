
import { Dimensions,Platform, PixelRatio } from 'react-native';
import {
  createStackNavigator,
  createSwitchNavigator,
  createBottomTabNavigator
} from "react-navigation";

import SplashScreen from "walku/src/scenes/Splash";
import LoginScreen from "walku/src/scenes/Sign/Login";
import StartScreen from 'walku/src/scenes/Home/scenes/Start';
import YourListScreen from 'walku/src/scenes/Home/scenes/YourList';
import AccountScreen from 'walku/src/scenes/Home/scenes/Account';
import AboutMeScreen  from 'walku/src/scenes/Home/scenes/AboutMe';
import MyProgramScreen  from 'walku/src/scenes/Home/scenes/MyProgram';
import MyProgram from '../scenes/Home/scenes/MyProgram';
import AboutUScreen  from 'walku/src/scenes/Home/scenes/AboutU';
import AboutU from '../scenes/Home/scenes/AboutU';

const {
  width: SCREEN_WIDTH,
} = Dimensions.get('window');

var scale = SCREEN_WIDTH/320;


 function normalize(size) {
  const newSize = size * scale 
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}

const GeneralNavigation = createStackNavigator({
  Home: {
    screen: createBottomTabNavigator({
      StartTab: {
        screen: StartScreen
      },
      YourListTab: {
        screen: YourListScreen
      },
      AccountTab: {
        screen: AccountScreen
      },
    }, {
     
        animationEnabled: false,
        swipeEnabled: false,
        tabBarPosition: 'bottom',
        initialLayout: false,
     
        tabBarOptions: {
          showIcon: true,
          showLabel: true,
          labelStyle: {

    fontSize: normalize(11),
          },
          style: {
            height: 52,
            backgroundColor: '#F8F9FB',
            paddingBottom: 4,
            borderTopColor: 'transparent',
            borderTopWidth: 1,
            // translateY:30
          },
         
          activeTintColor: '#FF5B35',
          inactiveTintColor: '#9DAEAB',
        }
      }), navigationOptions: ({ navigation }) => ({ header: null, }),
  },
  AboutMe:{ screen: AboutMeScreen,navigationOptions: ({ navigation }) => ({ header: null }) },
  MyProgram:{ screen: MyProgramScreen,navigationOptions: ({ navigation }) => ({ header: null }) },
  AboutU:{ screen: AboutUScreen,navigationOptions: ({ navigation }) => ({ header: null }) }
});

const AuthScreenNavigation = createStackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: ({ navigation }) => ({ header: null })
  },
});

const AppNavigator = (login)=>createSwitchNavigator(
  {
    Splash: {
      screen: SplashScreen,
      navigationOptions: ({ navigation }) => ({ header: null })
    },
    General: GeneralNavigation,
    AuthScreen: AuthScreenNavigation
  },
  { initialRouteName:login }
);

export default (AppNavigator);
