import React from 'react';
import { StyleSheet, View, Animated, Dimensions } from 'react-native';
import LogoAppoyarte from '../../components/Images/LOGO.png';
import { Container, Content } from 'native-base';
import ImageScalable from 'react-native-scalable-image';

export default class Splash extends React.Component {
  constructor() {
    super()
    this.state = {
      location: null,
      fadeAnim: new Animated.Value(0),
    }

  }

  componentWillMount() {
    this._AnimatedSplash();
  }
  componentDidMount() {
    this._setIntervalTime();
  }
  _setIntervalTime() {
    this.interval = setTimeout(() => {
      this.props.navigation.navigate("AuthScreen")
    }, 1000);
  }

  _AnimatedSplash() {
    Animated.timing(
      this.state.fadeAnim, // The value to drive
      { toValue: 1 }, // Configuration
    ).start();
  }
  render() {
    return (
      <Container>
        <Content style={styles.wrapper} contentContainerStyle={{ flex: 1 }}>
          <View style={styles.container}>
            <ImageScalable
              source={LogoAppoyarte}
              width={Dimensions.get('window').width -150 }
            />
          </View>
        </Content>

      </Container>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    resizeMode: 'cover',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
  },
  wrapper: {
    paddingRight: 24,
    paddingLeft: 24,
    paddingTop: 45,
    paddingBottom: 10,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});