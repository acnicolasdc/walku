import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Header, Icon, Left, Button, Body, Title, View } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';


export default class HeaderGray extends Component {
  constructor(props) {
    super(props);
    this.initialState = {
      title: this.props.title,
      color: this.props.colors,
      style: this.props.styles,
    };
    this.state = this.initialState;
  }
  render() {

    return (
      <View>
        <LinearGradient style={this.state.style ? styles.gradientHeader : styles.gradientHeaderGradient} start={{ x: 0.0, y: 0.0 }} end={{ x: 1.0, y: 1.0 }} colors={this.state.color}  >

          <Header androidStatusBarColor="transparent" transparent>


            <Left>
              <Button transparent onPress={() => this.props.navigation.goBack()}>
                <Icon name="arrow-back" style={{ color: '#ffff', marginLeft: 5 }} />
              </Button>
            </Left>
            <Body>
              <Title style={{ color: '#ffff' }}>{this.state.title}</Title>
            </Body>

          </Header>
        </LinearGradient>

      </View>

    );
  }
}
const styles = StyleSheet.create({
  gradientHeader: {
    // flex: 1,
    width: '100%',
    shadowColor: '#000000',
    elevation: 4,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 12,
    shadowOpacity: 0.5,
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12,
  },
  gradientHeaderGradient: {
    // flex: 1,
    width: '100%',
  }

});