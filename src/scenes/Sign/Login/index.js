import React, { PureComponent } from 'react';
import { StyleSheet, StatusBar,Dimensions } from 'react-native';
import { Container, Content,View , Footer,Text} from 'native-base';
//My components
import FormLogin from "./components/FormLogin";
import Background from "./components/Background";
import LinearGradient from 'react-native-linear-gradient';
import HeaderTwo from "../components/Header";
import ImageScalable from 'react-native-scalable-image';
import BannerUsb from 'walku/src/components/Images/banner2.jpg';

class Login extends PureComponent {

  componentDidMount() {
    StatusBar.setHidden(false);
  }
  render() {
    return (
      <Container>
        <LinearGradient style={{ flex: 1 }} start={{ x: 1.0, y: 1.0 }} end={{ x: 0.0, y: 0.0 }} colors={['#f9f9f9', '#f9f9f9']}  >      
          <View style={styles.header}>
          <ImageScalable
              source={BannerUsb}
              width={Dimensions.get('window').width}
            />
          </View>
              <Content style={styles.wrapper} contentContainerStyle={{ flex: 1 }}>
                <View style={styles.form}>
                  <FormLogin navigation={this.props.navigation} />
                </View>
              </Content>
              <Footer style={styles.footer}>
                  <Text style={{color:"#3ae374"}}>Power by Dionisio and Caipe</Text>
              </Footer>
       
        </LinearGradient>


      </Container>
    );
  }
}
const styles = StyleSheet.create({
  wrapper: {
    flex:3,
    paddingRight: 24,
    paddingLeft: 24,
    paddingTop: 0,
    paddingBottom: 0,
  
  },
  titulo: {
    flex: 1,
    marginBottom: 30,
    justifyContent: 'flex-end'
  },
  form: {
    flex: 4,
  },
  welcomeTitle: {
    fontSize: 28,
    fontFamily: 'GothamRounded-Bold',
    color: '#ffff',
  },
  header:{
    marginBottom: 30,
  },
  footer:{
      backgroundColor:'#f9f9f9'

  }

});
export default Login;



