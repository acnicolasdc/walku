import React, { Component } from 'react';
import { StyleSheet, ImageBackground } from 'react-native';

import LoginBackground from '../../images/LoginBackgroundDos.png';

class Background extends Component {
  render() {
    return (
      <ImageBackground style={styles.picture} source={LoginBackground}>
        {this.props.children}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  picture: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover',
  },
});

export default Background;
