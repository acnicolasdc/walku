import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Label, Input, Text, Spinner, Form, Item, Toast } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';


class FormLogin extends Component {
  constructor(props) {
    super(props);

    this.initialState = {
      isLoading: false,
      error: null,
      email: null,
      password: null,
      show: true,
    };
    this.state = this.initialState;
  }
  _toastAlert(message, type) {
    Toast.show({
      text: message,
      buttonText: "Cerrar",
      type: type
    })
  }
  render() {
    return (
      <Form style={styles.form}>        
          <Label style={styles.color} >Codigo estudiantil</Label>
          <Item regular style={styles.inputCode}>
            <Input style={{ color: '#b2bec3', }} keyboardType="numeric" autoCorrect={false} autoCapitalize="none" onChangeText={email => this.setState({ email })} />
          </Item>
        {this.state.isLoading ? (<Spinner size="small" color="#FFFFFF" />) :
          (
            <TouchableOpacity style={styles.buttonContainer} underlayColor='#ffff' activeOpacity={0.8} onPress={() =>  this.props.navigation.navigate("General")}>
              <LinearGradient style={styles.gradient} start={{ x: 0.0, y: 0.0 }} end={{ x: 1.0, y: 1.0 }} colors={['#ced6e0', '#ced6e0']}  >
                <Text style={styles.buttonTextB}> Ingresa</Text>
              </LinearGradient>
            </TouchableOpacity>
          )}
      </Form>
    );
  }
}
const styles = StyleSheet.create({
  form: {
    flex: 1,
    color: '#f9f9f9', alignItems: 'stretch',
  },
  color: {
    color: '#57606f', 
    fontSize: 20,
    fontFamily: 'GothamRounded-Bold',
    marginBottom:10
  },

  buttonContainer: {
    alignItems: 'center',
    marginTop: 20,
   
  },
  gradient: {
    // flex: 1,
    width: '100%',
    borderRadius: 10,
  },
  buttonTextB: {
    textAlign: 'center',
    color: '#FFF',
    fontSize: 17,
    padding: 15,
    marginLeft: 1,
    marginRight: 1,
    fontFamily: 'GothamRounded-Bold',
  },
  inputCode:{
    borderRadius:10,
    backgroundColor:"#FFF",
    color: '#b2bec3', 
  }

});

export default FormLogin;
