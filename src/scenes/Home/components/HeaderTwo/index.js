import React, { Component } from 'react';
import { StyleSheet,StatusBar} from 'react-native';
import {  Header, Icon, Left, Button, Body, Right, Title, View} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';


export default class HeaderGray extends Component {
  constructor(props) {
    super(props);
    this.initialState = { 
        title: this.props.title,
        color:this.props.colors,
        style:this.props.styles,
   };
    this.state = this.initialState;
  }
  render() {

    return (
        <View>
     <LinearGradient  style={this.state.style?styles.gradientHeader:styles.gradientHeaderGradient} start={{x: 0.0, y: 0.0}} end={{x: 1.0, y: 1.0}} colors={this.state.color}  >

          <Header style={{height:47+StatusBar.currentHeight}}  androidStatusBarColor="#D9D9D9" barStyle='light-content' transparent >
     

          <Left style={{flex:1}}>
            <Button transparent  onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" style={[styles.marginLeft]}/>
            </Button>
          </Left>
          <Body style={{flex:1, alignItems:'center'}}>
              <Title >{this.state.title}</Title>
          </Body>
           <Right style={{flex:1}} />
        </Header>
        </LinearGradient>      

        </View>
   
    );
  }
}
const styles = StyleSheet.create({
      gradientHeader:{
        width: '100%',
        shadowColor: '#000000',
        elevation:0,
        shadowOffset: {width: 0, height: 0}, 
        shadowRadius: 12,
        shadowOpacity: 0.5,
        borderBottomLeftRadius: 12,
      borderBottomRightRadius: 12,
      },
      gradientHeaderGradient:{
          width: '100%',
      },
      marginLeft:{
        marginLeft:5
      }
     
  });