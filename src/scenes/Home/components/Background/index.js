import React, { Component } from 'react';
import { StyleSheet, ImageBackground } from 'react-native';

import WelcomBackground from './images/backfondo.png';

class Background extends Component {
  render() {
    return (
      <ImageBackground style={styles.picture} source={WelcomBackground}>
        {this.props.children}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  picture: {
    flex: 1,
    width: null,
    height: null,
    
    resizeMode: 'cover',
  },
});

export default Background;
