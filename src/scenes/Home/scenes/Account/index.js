import React, { Component } from "react";
import { Container,Content,Icon,View } from "native-base";
//My components
import ListConfiguration from "./components/ListConfiguration";
import WelcomeUser from "./components/WelcomeUser";
import HeaderTwo from "../../components/HeaderTwo";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Background from "../../components/Background";

class Account extends Component {
  static navigationOptions = { tabBarIcon: ({ tintColor }) => {
      return (
        <View>    
        <FontAwesome
          name="user"
          size={18}
          style={{ color: tintColor,borderColor:'red', marginTop: 5 }}
        />
        </View>
      );
    },
    title: "Tu cuenta"
  };

  render() {
    return (
      <Container>
        <HeaderTwo
          colors={["#FFFF", "#FFFF"]}
          title={""}
          navigation={this.props.navigation}
          styles={true}
        />
        
        <Content>
          <Background>
        <WelcomeUser
          navigation={this.props.navigation}
        />
        <ListConfiguration navigation={this.props.navigation} />
        </Background>
        </Content>
      </Container>
    );
  }
}

export default (Account);
