import React, { Component } from "react";
import { StyleSheet, Alert, View } from "react-native";
import { List, ListItem, Text, Left, Right } from "native-base";
import Icon from "react-native-vector-icons/dist/SimpleLineIcons";
import Entypo from "react-native-vector-icons/Entypo";




class ListConfiguration extends Component {
logOut(){
  Alert.alert(
    'Cerrar sesion',
    '¿Estas seguro?',
    [
      {
        text: 'Cancelar',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'Si', onPress: () =>this.props.navigation.navigate("Login")},
    ],
    {cancelable: false},
  );
}
  render() {
    return (
      <List>
            <ListItem style={styles.listItemStyle}
            onPress={()=> this.props.navigation.navigate("AboutMe")}>
          <Left style={{ alignItems: "center" }}>
            <View style={styles.borderIcon}>
              <Entypo name="info" size={15} color={"#f0932b"} />
            </View>
            <Text
              style={[

                styles.textList
              ]}
            >
              Sobre Mi
            </Text>
          </Left>
          <Right>
            <Icon
              name="arrow-right"
              size={14}
              style={[styles.iconSpace,]}
            />
          </Right>
        </ListItem>
        <ListItem style={styles.listItemStyle}
        onPress={()=> this.props.navigation.navigate("MyProgram")}>
          <Left style={{ alignItems: "center" }}>
            <View style={styles.borderIcon}>
              <Entypo name="bookmarks" size={15} color={"#f0932b"} />
            </View>
            <Text
              style={[

                styles.textList
              ]}
            >
              Mi Programa
            </Text>
          </Left>
          <Right>
            <Icon
              name="arrow-right"
              size={14}
              style={[styles.iconSpace,]}
            />
          </Right>
        </ListItem>
       
        <ListItem style={styles.listItemStyle}
        onPress={()=> this.props.navigation.navigate("AboutU")}>
          <Left style={{ alignItems: "center" }}>
            <View style={styles.borderIcon}>
              <Icon name="help" size={15} color={"#f0932b"} />
            </View>
            <Text
              style={[

                styles.textList
              ]}
            >
              Sobre la U
            </Text>
          </Left>
          <Right>
            <Icon
              name="arrow-right"
              size={14}
              style={[styles.iconSpace,]}
            />
          </Right>
        </ListItem>
        <ListItem style={styles.listItemStyle}
        onPress={()=> this.logOut()}>
          <Left style={{ alignItems: "center" }}>
            <View style={styles.borderIcon}>
              <Icon name="logout" size={15} color={"#f0932b"} />
            </View>
            <Text
              style={[
                styles.textList
              ]}
            >
              Cerrar Sesión
            </Text>
          </Left>
          <Right>
            <Icon
              name="arrow-right"
              size={14}
              style={[styles.iconSpace,]}
            />
          </Right>
        </ListItem>
      </List>
    );
  }
}
const styles = StyleSheet.create({
  wrapper: {
    paddingRight: 10,
    paddingLeft: 10
  },
  listItemStyle: {
  
    borderBottomWidth: 0,
    height: 50
  },
  borderIcon: {
    borderColor: "#f0932b",
    height: 30,
    width: 30,
    borderWidth: 0.5,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  textList: {
    marginVertical: 10,
    marginLeft: 20,
    color:"#576574"
  }
});

export default (ListConfiguration);
