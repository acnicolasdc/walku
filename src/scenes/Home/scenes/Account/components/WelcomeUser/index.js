import React, {Component} from 'react';
import {Text,View, Item,H1, Icon, Image} from 'native-base';
import { StyleSheet } from 'react-native';
import ImageScalable from "react-native-scalable-image";
import FontAwesome from "react-native-vector-icons/FontAwesome5";
import Entypo from "react-native-vector-icons/Entypo";






//global styles Importation

class WelcomeUser extends Component { 

  render() {
    return (
        <View style={styles.wrapper}>



        
        <ImageScalable 
        source={require("../../images/maleUser.png")}
              width={58} />
               
       
        <View style={{marginLeft:25}}>
        <H1 style={[styles.welcomeTitle]}>¡Hola Sebastian!</H1>

        <Item style={{flexDirection:'row',alignItems:'center', borderColor:'#ffff'} }>       
         <FontAwesome name='code-branch' style={[styles.settings]}/>

         <Text style={[styles.options]} > Codigo: 1140171</Text>
        
        </Item>
        
        
         
        </View>
        </View>
    );
  }
}
const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#F8F9FB',
    paddingRight:20,
    paddingLeft:20,
    flexDirection:'row',
    alignItems:'center'
  },
  welcomeTitle:{
    marginBottom:10,
    marginTop:35,
    
    alignItems:'center',
    color:"#f0932b"
  },
  options:{
    marginBottom:10,
    
    color:"#576574"
  },
  settings:{

    fontSize:18,marginRight:7,marginLeft:7,marginBottom:10,color:"#f0932b"
  }
});


export default (WelcomeUser);

