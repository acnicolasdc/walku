import React, { Component } from "react";
import { StyleSheet,View } from "react-native";
import { Container, Content, H1,Header,List, ListItem, Text, Accordion, Icon } from "native-base";
import FontAwesome from "react-native-vector-icons/FontAwesome";

const dataArray = [
  { title: "Actividad uno", content: "Lorem ipsum dolor sit amet" },
  { title: "Actividad dos", content: "Lorem ipsum dolor sit amet" },
  { title: "Actividad tres", content: "Lorem ipsum dolor sit amet" },
  { title: "Actividad 4", content: "Lorem ipsum dolor sit amet" }
];



class YourList extends Component { 



  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => {

      return (
        <View>
        <FontAwesome
          name="heart"
          size={18}
          style={{ color: tintColor, marginTop: 5 }}
        />
        </View>
      );
    },
    title: "Actvidades"
  };

  render() {
    return (
      <Container>

        <Content style={styles.wrapper}>
        <Header  androidStatusBarColor="#D9D9D9" barStyle='light-content' transparent style={{backgroundColor:'#F8F9FB',justifyContent:'flex-start',alignItems:'center'}}>
          <H1 style={[styles.welcomeTitle]}>
            Mis actividades
          </H1>
          </Header>
          <View style={[styles.color]}>
          <Accordion
            dataArray={dataArray}
            animation={true}
            expanded={true}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
          />
          
          </View>
          
        </Content>
        
        
        
        
      </Container>
    );
  }
  _renderHeader(item, expanded) {
  return (
    <View style={{
      flexDirection: "row",
      padding: 10,
      justifyContent: "space-between",
      alignItems: "center" ,
      borderRadius: 3,
      backgroundColor: "#F79F1F" }}>
    <Text style={{ 
      fontWeight: "600",
      color:"white"
      
      }}>
        {" "}{item.title}
      </Text>
      {expanded
        ? <Icon style={{ fontSize: 18, borderRadius: 10}} name="remove-circle" />
        : <Icon style={{ fontSize: 18,borderRadius: 10 }} name="add-circle" />}
    </View>
  );
}

  _renderContent(item) {
    return (
      <Text
        style={{
          backgroundColor: "#e3f1f1",
          
          padding: 10,
          fontStyle: "italic",
        }}
      >
        {item.content}
      </Text>
    );
  }

}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: "#ffff",
  },
  welcomeTitle: {
    marginVertical: 5,
    paddingHorizontal:10
  },
  color:{
    backgroundColor:"white",
    height:300   


  }
});

export default (YourList);

