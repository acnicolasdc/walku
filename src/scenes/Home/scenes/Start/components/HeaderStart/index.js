import React, { Component } from 'react';
import { StyleSheet,StatusBar} from 'react-native';
import {  Header, Button, Body, Text} from 'native-base';

 class HeaderStart extends Component {      
 

  render() {

    return (
        
        <Header style={{height:47+StatusBar.currentHeight}} androidStatusBarColor="#D9D9D9" barStyle='light-content' transparent>
            <Body style={{flex:2, alignItems:"center"}}>
              <Button
                iconRight
                transparent
              >
                <Text uppercase={false}>
                  Bienvenido
                </Text>
              </Button>
            </Body>

          </Header>
   
    );
  }
}
  
  export default (HeaderStart)