import React, { PureComponent } from "react";
import {  Icon,View} from "react-native";
import { Container} from "native-base";
import HeaderStart from './components/HeaderStart';
import FontAwesome from "react-native-vector-icons/FontAwesome";

class Start extends PureComponent { 
  static navigationOptions = ({ navigation }) => {

    return {
    tabBarIcon: ({ tintColor }) => {
      return (
          <FontAwesome name="home" size={18} style={{ color: tintColor, marginTop: 5 }} />
      );
    },
    title: "Inicio"
    }
  };

  render() {
 
    return (
      <Container>
        <HeaderStart  navigation={this.props.navigation}/>
        <View>

        </View>
      </Container>
    );
  }
}

export default (Start);
